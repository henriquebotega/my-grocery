import { MaterialModule } from "./material/material/material.module";
import { HttpClientModule } from "@angular/common/http";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ListComponent } from "./components/list/list.component";
import { CartComponent } from "./components/cart/cart.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ModalAddProductComponent } from "./components/modal-add-product/modal-add-product.component";

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    CartComponent,
    ModalAddProductComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MaterialModule,
    BrowserAnimationsModule
  ],
  providers: [],
  entryComponents: [ModalAddProductComponent],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
