import { Product } from "./../../interfaces/Product";
import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

@Component({
  selector: "app-modal-add-product",
  templateUrl: "./modal-add-product.component.html",
  styleUrls: ["./modal-add-product.component.css"]
})
export class ModalAddProductComponent {
  unit_weight: string;

  constructor(
    public dialogRef: MatDialogRef<ModalAddProductComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Product
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
