import { ModalAddProductComponent } from "./../modal-add-product/modal-add-product.component";
import { Product, typePromotion, priceType } from "./../../interfaces/Product";
import { ListService } from "./list.service";
import { Component, OnInit } from "@angular/core";
import { MatDialog, MatSnackBar } from "@angular/material";

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.css"]
})
export class ListComponent implements OnInit {
  items: Product[];

  constructor(
    private _snackBar: MatSnackBar,
    private listService: ListService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.listService.getAll().subscribe((res: [Product]) => {
      this.items = res;
    });
  }

  openDialog(prod: Product) {
    const dialogRef = this.dialog.open(ModalAddProductComponent, {
      width: "350px",
      data: prod
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.listService.addProduct(prod, result);
        this.openSnackBar("Your product has been successfully added!");
      }
    });
  }

  buy(prod: Product) {
    if (prod.priceBy == priceType.KG) {
      this.openDialog(prod);
    } else {
      this.listService.addProduct(prod, 1);
      this.openSnackBar("Your product has been successfully added!");
    }
  }

  showPromotion(prod: Product) {
    if (prod.promotion == typePromotion.BUYONE_GETONE) {
      return "BUY ONE & GET ONE";
    }
  }

  showPrice(prod: Product) {
    if (prod.priceBy == priceType.UNIT) {
      return `$${prod.price} each`;
    }

    if (prod.priceBy == priceType.KG) {
      return `$${prod.price}/kg`;
    }
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, "OK", {
      duration: 3000
    });
  }
}
