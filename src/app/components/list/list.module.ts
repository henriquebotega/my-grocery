import { MaterialModule } from './../../material/material/material.module';
import { ListService } from './list.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MaterialModule
  ],
  providers:[
    ListService
  ]
})
export class ListModule { }
