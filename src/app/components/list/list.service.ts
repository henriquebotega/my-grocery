import { Item } from "./../../interfaces/Item";
import { Product, typePromotion } from "./../../interfaces/Product";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EventEmitter, Output } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ListService {
  @Output() msgChanged: EventEmitter<any> = new EventEmitter();
  @Output() couponChanged: EventEmitter<any> = new EventEmitter();
  private items: Item[] = [];

  constructor(private httpClient: HttpClient) {}

  getAll() {
    return this.httpClient.get("assets/data.json");
  }

  getId(items): number {
    let lastItem = items[items.length - 1];
    let lastID = lastItem ? lastItem["id"] : 0;

    return lastID + 1;
  }

  addProduct(prod: Product, qtt: number) {
    this.items = JSON.parse(localStorage.getItem("cart")) || [];

    let item = {
      id: this.getId(this.items),
      product: prod,
      quantity: qtt,
      total: parseFloat((prod.price * qtt).toFixed(2)),
      promotion: {
        isFree: false,
        itemFather: null
      }
    };

    this.items.push(item);

    if (prod.promotion == typePromotion.BUYONE_GETONE) {
      let itemFree = {
        id: this.getId(this.items),
        product: prod,
        quantity: qtt,
        total: 0,
        promotion: {
          isFree: true,
          itemFather: item
        }
      };

      this.items.push(itemFree);
    }

    localStorage.setItem("cart", JSON.stringify(this.items));
    this.msgChanged.emit({
      total: this.getTotalItems(),
      totalValue: this.getTotalValue(),
      coupon: this.getCoupon(),
      items: this.items
    });
  }

  removeProduct(item: Item) {
    this.items = JSON.parse(localStorage.getItem("cart")) || [];
    let col = [...this.items.filter((i: Item) => i.id != item.id)];

    let colItems = [
      ...col.filter((i: Item) =>
        i.promotion.itemFather ? i.promotion.itemFather.id != item.id : i
      )
    ];

    localStorage.setItem("cart", JSON.stringify(colItems));

    let currentTotalValue = this.getTotalValue();

    let coupon = JSON.parse(localStorage.getItem("coupon")) || null;
    if (coupon && currentTotalValue < coupon.min) {
      localStorage.removeItem("coupon");

      this.couponChanged.emit({
        totalValue: currentTotalValue,
        coupon: this.getCoupon()
      });
    }

    this.msgChanged.emit({
      total: this.getTotalItems(),
      totalValue: currentTotalValue,
      coupon: this.getCoupon(),
      items: colItems
    });
  }

  applyDiscount(couponDiscount) {
    localStorage.setItem("coupon", JSON.stringify(couponDiscount));

    this.couponChanged.emit({
      totalValue: this.getTotalValue(),
      coupon: this.getCoupon()
    });
  }

  clearAll() {
    localStorage.removeItem("cart");
    localStorage.removeItem("coupon");
    this.msgChanged.emit({ total: 0, totalValue: 0, coupon: null, items: [] });
  }

  getCoupon() {
    return JSON.parse(localStorage.getItem("coupon")) || null;
  }

  getProducts(): Item[] {
    return JSON.parse(localStorage.getItem("cart")) || [];
  }

  getTotalItems(): number {
    return this.getProducts().length;
  }

  getTotalValue(): number {
    return parseFloat(
      this.getProducts()
        .reduce((a, b) => {
          return a + b["total"];
        }, 0)
        .toFixed(2)
    );
  }
}
