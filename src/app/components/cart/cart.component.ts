import { ListService } from "./../list/list.service";
import { Item } from "./../../interfaces/Item";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { MatSnackBar } from "@angular/material";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";

@Component({
  selector: "app-cart",
  templateUrl: "./cart.component.html",
  styleUrls: ["./cart.component.css"]
})
export class CartComponent implements OnInit, OnDestroy {
  items: Item[];
  total: number = 0;
  couponSaved = null;
  couponNumber: string;

  colCoupons = [
    {
      code: "5off",
      discount: 5,
      min: 100
    },
    {
      code: "10off",
      discount: 10,
      min: 150
    }
  ];

  _serviceSubscription: Subscription;
  _serviceSubscriptionCoupon: Subscription;

  displayedColumns: string[] = [
    "img",
    "id",
    "title",
    "price",
    "quantity",
    "total",
    "actions"
  ];

  constructor(
    private _snackBar: MatSnackBar,
    private router: Router,
    private listService: ListService
  ) {}

  ngOnInit() {
    this.items = this.listService.getProducts();
    this.total = this.listService.getTotalValue();
    this.couponSaved = this.listService.getCoupon();

    this._serviceSubscription = this.listService.msgChanged.subscribe(res => {
      this.total = res["totalValue"];
      this.items = res["items"];
      this.couponSaved = res["coupon"];
    });

    this._serviceSubscriptionCoupon = this.listService.couponChanged.subscribe(
      res => {
        this.total = res["totalValue"];
        this.couponSaved = res["coupon"];
      }
    );
  }

  remove(item: Item) {
    this.listService.removeProduct(item);
    this.openSnackBar("Your product was successfully removed!");
  }

  checkout() {
    this.listService.clearAll();
    this.openSnackBar("Your order was successfully finished!");
    this.router.navigate(["/list"]);
  }

  openSnackBar(message: string, duration?: number) {
    this._snackBar.open(message, "OK", {
      duration: duration ? duration : 2000
    });
  }

  isCouponValid() {
    let currentCoupon = this.colCoupons.find(obj => obj.code == this.couponNumber);

    if (currentCoupon) {
      if (this.total > currentCoupon.min) {
        this.openSnackBar(`You earned ${currentCoupon.discount}% discount!`);
        this.listService.applyDiscount(currentCoupon);
      } else {
        this.openSnackBar(
          `This coupon is valid, but you need to buy over $${currentCoupon.min} to receive the discount!`,
          5000
        );
      }
    } else {
      this.openSnackBar("This coupon is invalid!");
    }
  }

  showTotal() {
    if (this.couponSaved) {
      let disc = (this.couponSaved.discount * 100) / this.total;
      return parseFloat((this.total - disc).toFixed(2));
    } else {
      return this.total;
    }
  }

  ngOnDestroy() {
    this._serviceSubscription.unsubscribe();
    this._serviceSubscriptionCoupon.unsubscribe();
  }
}
