import { ListService } from "./components/list/list.service";
import { Component, OnInit, EventEmitter, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit, OnDestroy {
  title = "myGrocery";
  totalCart: number = 0;
  _serviceSubscription: Subscription;

  constructor(private listService: ListService) {}

  ngOnInit() {
    this.totalCart = this.listService.getTotalItems();

    this._serviceSubscription = this.listService.msgChanged.subscribe(res => {
      this.totalCart = res["total"];
    });
  }

  ngOnDestroy() {
    this._serviceSubscription.unsubscribe();
  }
}
