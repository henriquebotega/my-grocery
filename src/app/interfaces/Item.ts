import { Product } from "./Product";

export interface Item {
  id: number;
  product: Product;
  quantity: number;
  total: number;
  promotion: {
    isFree: boolean;
    itemFather: Item;
  };
}
