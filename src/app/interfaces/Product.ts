export enum priceType {
  UNIT = "UNIT",
  KG = "KG"
}
export enum typePromotion {
  NONE = "NONE",
  BUYONE_GETONE = "BUYONE_GETONE"
}

export interface Product {
  id: number;
  title: string;
  description: string;
  img: string;
  price: number;
  priceBy: priceType;
  promotion: typePromotion;
}
